const mongoose = require("mongoose")

const course_schema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course name is required"]
	}, 
	description: {
		type: String,
		required: [true, "Course description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: false
	}, 
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [ // The braces [] signify that this field will contain multiple data
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("Course", course_schema)
